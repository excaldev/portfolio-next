import { SiJavascript, SiLaravel, SiNextdotjs, SiNodedotjs, SiPhp } from "react-icons/si";

const Languages = {
    Laravel: {
        color: "#FF2D20",
        icon: <SiLaravel className="w-4 h-4 mr-1 text-[#FF2D20]"></SiLaravel>,
        label: "Laravel"
    },
    PHP: {
        color: "#777BB4",
        icon: <SiPhp className="w-4 h-4 mr-1 text-[#777BB4]"></SiPhp>,
        label: "PHP Native"
    },
    Javascript: {
        color: "#F7DF1E",
        icon: <SiJavascript className="w-4 h-4 mr-1 text-[#F7DF1E]"></SiJavascript>,
        label: "Javascript"
    },
    Node: {
        color: "#339933",
        icon: <SiNodedotjs className="w-4 h-4 mr-1 text-[#339933]"></SiNodedotjs>,
        label: "Node JS"
    },
    Next: {
        color: "#000000",
        icon: <SiNextdotjs className="w-4 h-4 mr-1 text-[#000000]"></SiNextdotjs>,
        label: "Next JS"
    },
}

interface WorksProps {
    name: string;
    description: string;
    url: string;
    image: string;
    language: "Laravel" | "PHP" | "Javascript" | "Node" | "Next";
}

const PortfolioItem = ({ name, description, url, image, language }: WorksProps) => {

    return (
        <a href={url} rel="noreferrer" target="_blank">
            <div className="max-w-sm rounded overflow-hidden shadow-lg bg-white dark:bg-gray-800">
                <img className="w-full" src="/assets/background.jpg" alt="Sunset in the mountains" />
                <div className="px-6 py-4">
                    <div className="font-bold text-xl mb-2 text-gray-800 dark:text-white">{name}</div>
                    <p className="text-gray-700 text-base dark:text-gray-300">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus quia, nulla! Maiores et perferendis eaque, exercitationem praesentium nihil.
                    </p>
                </div>
                <div className="px-6 pt-4 pb-2">
                    <span className="inline-flex bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">{Languages["Laravel"].icon} {Languages["Laravel"].label} </span>
                    <span className="inline-flex bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">{Languages["Node"].icon} {Languages["Node"].label} </span>
                    <span className="inline-flex bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">{Languages["Next"].icon} {Languages["Next"].label} </span>
                    <span className="inline-flex bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">{Languages["PHP"].icon} {Languages["PHP"].label} </span>
                </div>
            </div>

        </a>

    );
}
export default PortfolioItem;

