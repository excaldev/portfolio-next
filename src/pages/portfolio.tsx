import { motion } from "framer-motion";
import PortfolioItem from "../components/PortfolioItem";
import RepoItem from "../components/RepoItem";
const Portfolio = () => {
    return (
        <motion.div
            initial={{ opacity: 0, scale: 0.95 }}
            animate={{ opacity: 1, scale: 1 }}
            exit={{ opacity: 0, scale: 0.95 }}
            transition={{ ease: "easeOut", duration: 0.15 }}
            className="mt-24 w-full mb-32"
        >
            <h1 className="mt-36 font-bold text-4xl md:text-5xl mb-4">My Works</h1>
            <p className="text-gray-800 dark:text-gray-300 leading-6 font-light tracking-wide mb-6">
                Please find below a selection of my recent works in web development. These projects showcase my skills
                and expertise in the field, and I am excited to share them with you.
            </p>
            <div className="w-full grid grid-cols-1 md:grid-cols-2 grid-rows-2 md:grid-rows-1 mb-12 gap-12">
                <PortfolioItem
                    key={"a"}
                    name={"a"}
                    description={"a"}
                    url={"a"}
                    image={"a"}
                    language={"Javascript"}

                />
            </div>
        </motion.div>
    );
};

export default Portfolio;
